import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T> {

  constructor(
    protected http:HttpClient,
    @Inject("url") protected url:string
  ) { }


  listar() {
    return this.http.get<T[]>(this.url);

  }

  listarPorId(id:number) {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  /*registrar(paciente:Paciente) {
    return this.http.post<Paciente>(`${this.url}`, paciente);
  }*/

  registrar(paciente: T) {
    return this.http.post(`${this.url}`, paciente);
  }

  modificar(paciente:T) {
    return this.http.put(`${this.url}`, paciente);
  }

  eliminar(id:number) {
    return this.http.delete(`${this.url}/${id}`);
  }


}
