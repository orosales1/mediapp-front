import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Menu } from '../_model/menu';
import { LoginService } from './login.service';
import { MenuService } from './menu.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(
    private loginService: LoginService,
    private menuService: MenuService,
    private router: Router
  ) { }

  canActivate( route: ActivatedRouteSnapshot,  state: RouterStateSnapshot) {
         // Verify your login
         let rpta = this.loginService.estaLogueado();

         if (!rpta) {
           this.loginService.cerrarSession();
           return false;
         }
         // Verify the expired token
         const helper = new JwtHelperService();
         let token = sessionStorage.getItem(environment.TOKEN_NAME );
         if (!helper.isTokenExpired(token)) {
            // Verify if you have the granted roles with access to the path ( page )

            // Obtaining the URL  url-> /pages/consulta
           let url = state.url;
           const decodedToken = helper.decodeToken(token);

           // When you use a guard, never use a suscribe, you need a pipe because it should not be asynchronous
           return this.menuService.listarPorUsuario(decodedToken.user_name).pipe(map((data: Menu[]) => {
            this.menuService.setMenuCambio(data);

            let cont = 0;
            for (let m of data) {
              if (url.startsWith(m.url)) {
                cont++;
                break;
              }
            }

            if (cont > 0) {
              return true;
            } else {
              this.router.navigate(['/pages/not-403']);
              return false;
            }

          }));

         } else {
          this.loginService.cerrarSession();
          return false;
         }



        return true;
     }
}
