import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaWizardComponent } from './consulta-wizard.component';

describe('ConsultaWizardComponent', () => {
  let component: ConsultaWizardComponent;
  let fixture: ComponentFixture<ConsultaWizardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaWizardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
