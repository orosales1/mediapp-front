import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';
import * as moment from 'moment';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  id : number = 0;
  edicion: boolean = false;

  pacientes$: Observable<Paciente[]>;

  idPacienteSeleccionado: number;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  constructor(private pacienteService: PacienteService,
    private route:ActivatedRoute,
    private signosService: SignosService,
    private router:Router ) { }

  ngOnInit(): void {
      this.form = new FormGroup({
        'id' : new FormControl(0),
        'idPaciente': new FormControl(0),
        'fecha': new FormControl(''),
        'pulso':  new FormControl(''),
        'temperatura': new FormControl(''),
        'ritmoRespiratorio': new FormControl('')
      });

      this.pacientes$ = this.pacienteService.listar(); //.subscribe(data => this.pacientes = data);

      this.route.params.subscribe((data: Params) => {
        this.id = data['id'];
        this.edicion = data['id'] != null;
        this.initForm();
      });
  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSignos),
          'idPaciente': new FormControl(data.paciente.idPaciente),
          'fecha': new FormControl(data.fecha),
          'pulso': new FormControl(data.pulso),
          'temperatura': new FormControl(data.temperatura),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)

        });
      });
    }
  }

  operar() {
    let signos = new Signos();
    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['idPaciente'];
    signos.paciente = paciente;

    signos.idSignos = this.form.value['id'];
    signos.fecha = moment( this.form.value['fecha'] ).format('YYYY-MM-DDTHH:mm:ss');
    signos.pulso = this.form.value['pulso'];
    signos.temperatura = this.form.value['temperatura'];
    signos.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    console.log(signos);

    if (this.edicion) {
      console.log("Modify");
      this.signosService.modificar(signos).pipe( switchMap( () => {
        return this.signosService.listar();
      })).subscribe( data => {
          this.signosService.setSignoCambio(data);
          this.signosService.setMensajeCambio("Se Modifico");
      })
    }
      else {

        this.signosService.registrar(signos).pipe( switchMap( () =>  {
          return this.signosService.listar();
      }) ).subscribe( data => {
             this.signosService.setSignoCambio(data);
             this.signosService.setMensajeCambio('SE REGISTRO');
           }
      );

    }

    this.router.navigate(['/pages/signos']);

  }

}
